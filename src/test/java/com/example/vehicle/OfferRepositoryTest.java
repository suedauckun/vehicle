package com.example.vehicle;

import controller.OfferController;
import entity.Offer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import repo.OfferRepository;
import repo.SaleRepository;
import service.OfferService;

import java.util.List;
import java.util.Optional;

import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

@DataJpaTest
public class OfferRepositoryTest {
    @Mock
    OfferRepository offerRepository;

    @Test
    public void testSaveOffer() {
        Offer offer=Offer.builder()
                .offerId(1L)
                .offerTanim("Test bir")
                .vehicle_Id(1L)
                .fiyat(1500000.45)
                .build();

        offerRepository.save(offer);
        Assertions.assertThat(offer.getOfferId()).isGreaterThan(0L);
    }
    @Test
    public void testGetOfferByUserId(){
        Offer offer=offerRepository.findById(1L).get();
        Assertions.assertThat(offer.getOfferId()).isEqualTo(1L);
    }

    @Test
    public void updateOfferTest(){

        Offer offer = offerRepository.findById(1L).get();

        offer.setFiyat(1754000.00);

        Offer offerUpdated =  offerRepository.save(offer);

        Assertions.assertThat(offerUpdated.getFiyat()).isEqualTo("1754000.00");

    }

    @Test
    public void deleteOfferByIdTest(){

        Offer offer = offerRepository.findById(1L).get();

        offerRepository.delete(offer);

        Offer offer1 = null;

        Optional<Offer> optionalEmployee = offerRepository.findById(1L);

        if(optionalEmployee.isPresent()){
            offer1 = optionalEmployee.get();
        }

        Assertions.assertThat(offer1).isNull();
    }
}
