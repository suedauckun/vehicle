package repo;

import entity.Offer;
import entity.Sale;
import entity.User;
import entity.Vehicle;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.ArrayList;
import java.util.List;

public interface SaleRepository extends JpaRepository<Sale,Long> {

    @Query(value = "SELECT vehicle_id, COUNT(*) AS total_sales FROM Sale GROUP BY vehicle_id ORDER BY total_sales DESC", nativeQuery = true)
    List<Vehicle> findMostPurchasedVehicles();

    @Query("SELECT s.vehicle.id FROM Sale s WHERE s.customer.id = :customerId")
    List<Vehicle> seeCustomerProduct(@Param("userId") Long userId);

}
