package service;

import entity.AdditionalOffer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import repo.AdditionalOfferRepository;
import repo.OfferRepository;
import entity.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferService {

    @Autowired
    OfferRepository offerRepository;

    public Offer save(Offer offer){
        return offerRepository.save(offer);
    }

    public List<Offer> getAllOffer(){
        return offerRepository.findAll();
    }
    /*kullanıcılar teklifleri görebilir */
    public Offer getOfferByUserId(Long userId){
        return offerRepository.findById(userId).get();
    }

    public Offer updateOffer(Offer offer){
        return offerRepository.save(offer);
    }

    public void deleteOfferById(Long offerId){
        offerRepository.deleteById(offerId);
    }


}
