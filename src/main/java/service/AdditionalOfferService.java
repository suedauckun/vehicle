package service;

import entity.AdditionalOffer;
import entity.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repo.AdditionalOfferRepository;

import java.util.List;

@Service
public class AdditionalOfferService {

    @Autowired
    AdditionalOfferRepository additionalOfferRepository;

    public AdditionalOffer save(AdditionalOffer additionalOffer){
        return additionalOfferRepository.save(additionalOffer);
    }

    public List<AdditionalOffer> getAllOffer(){
        return additionalOfferRepository.findAll();
    }

    public AdditionalOffer updateOffer(AdditionalOffer additionalOffer){
        return additionalOfferRepository.save(additionalOffer);
    }

    public void deleteOfferById(Long additionalOfferId){
        additionalOfferRepository.deleteById(additionalOfferId);
    }
}
