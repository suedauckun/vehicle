package service;

import entity.Offer;
import entity.User;
import entity.UserRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repo.UserRepository;
import repo.UserRoleRepository;

import java.util.List;

@Service
public class UserRolesService {
    @Autowired
    UserRoleRepository userRoleRepository;

    @Autowired OfferService offerService;

    public UserRoles save(UserRoles userRoles){
        return userRoleRepository.save(userRoles);
    }

    public UserRoles updateUserRoles(UserRoles userRoles){
        return userRoleRepository.save(userRoles);
    }

    public void deleteUserRoleById(Long userRoleId){
        userRoleRepository.deleteById(userRoleId);
    }

    public List<UserRoles> getUserRole(Long userId) {
        return userRoleRepository.findAllById(userId);
    }
}
