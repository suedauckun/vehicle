package entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;

@Entity
@Data
@Builder
@Table(name="Offer")
public class Offer {

    @Id
    @SequenceGenerator(initialValue = 1, name = "idGenerator", sequenceName = "SEQ_Offer")
    @GeneratedValue(generator = "idGenerator", strategy = GenerationType.AUTO)
    @Column(name = "OFFER_ID", unique = true, nullable = false, columnDefinition = "long")
    private long offerId;

    @Column(name ="OFFER_TANIM",columnDefinition = "varchar(50)")
    private String offerTanim;

    @Column(name ="FIYAT",columnDefinition = "double")
    private Double fiyat;

    @Column(name ="VEHICLE_ID",columnDefinition = "long")
    private Long vehicle_Id;


    public Offer() {
    }

    public Offer(long offerId, String offerTanim, Double fiyat, Long vehicle_Id) {
        this.offerId = offerId;
        this.offerTanim = offerTanim;
        this.fiyat = fiyat;
        this.vehicle_Id = vehicle_Id;
    }

    public long getOfferId() {
        return offerId;
    }

    public void setOfferId(long offerId) {
        this.offerId = offerId;
    }

    public String getOfferTanim() {
        return offerTanim;
    }

    public void setOfferTanim(String offerTanim) {
        this.offerTanim = offerTanim;
    }

    public Long getVehicle_Id() {
        return vehicle_Id;
    }

    public void setVehicle_Id(Long vehicle_Id) {
        this.vehicle_Id = vehicle_Id;
    }

    public Double getFiyat() {
        return fiyat;
    }

    public void setFiyat(Double fiyat) {
        this.fiyat = fiyat;
    }
}
