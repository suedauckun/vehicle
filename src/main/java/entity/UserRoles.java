package entity;

import jakarta.persistence.*;

@Table(name="UserRoles")
@Entity
public class UserRoles {

    @Id
    @SequenceGenerator(initialValue = 1, name = "idGenerator", sequenceName = "SEQ_UserRoles")
    @GeneratedValue(generator = "idGenerator", strategy = GenerationType.AUTO)
    @Column(name = "USER_ROLES_ID", unique = true, nullable = false, columnDefinition = "long")
    private Long userRolesId;

    @Column(name = "USER_ID", columnDefinition = "long")
    private Long userId;

    @Column(name = "KULLANICI_ROL_AD", length = 50, columnDefinition = "varchar(50)")
    private String kullaniciRolAd;

    @Column(name = "KULLANICI_ROL_KOD", length = 50, columnDefinition = "varchar(50)")
    private String kullaniciRolKod;

    public UserRoles() {
    }

    public Long getUserRolesId() {
        return userRolesId;
    }

    public void setUserRolesId(Long userRolesId) {
        this.userRolesId = userRolesId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getKullaniciRolAd() {
        return kullaniciRolAd;
    }

    public void setKullaniciRolAd(String kullaniciRolAd) {
        this.kullaniciRolAd = kullaniciRolAd;
    }

    public String getKullaniciRolKod() {
        return kullaniciRolKod;
    }

    public void setKullaniciRolKod(String kullaniciRolKod) {
        this.kullaniciRolKod = kullaniciRolKod;
    }
}
