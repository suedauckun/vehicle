package entity;

import jakarta.persistence.*;

@Entity
@Table(name="AdditionalOffer")
public class AdditionalOffer extends Offer{

    @Id
    @SequenceGenerator(initialValue = 1, name = "idGenerator", sequenceName = "SEQ_AdditionalOffer")
    @GeneratedValue(generator = "idGenerator", strategy = GenerationType.AUTO)
    @Column(name = "AdditionalOfferId", unique = true, nullable = false, columnDefinition = "long")
    private Long additionalOfferId;

    @Column(name ="extraOzellik",columnDefinition = "varchar(100)")
    private String extraOzellik;

    @Column(name ="extraFiyat",columnDefinition = "double")
    private Double extraFiyat;

    @Column(name ="VEHICLE_ID",columnDefinition = "long")
    private Long vehicle_Id;

    @Column(name ="OfferId",columnDefinition = "long")///*hangi teklife ait ek teklif sunulacak *////
    private Long offer_Id;

    public AdditionalOffer() {
    }

    public Long getAdditionalOfferId() {
        return additionalOfferId;
    }

    public void setAdditionalOfferId(Long additionalOfferId) {
        this.additionalOfferId = additionalOfferId;
    }

    public String getExtraOzellik() {
        return extraOzellik;
    }

    public void setExtraOzellik(String extraOzellik) {
        this.extraOzellik = extraOzellik;
    }

    public Double getExtraFiyat() {
        return extraFiyat;
    }

    public void setExtraFiyat(Double extraFiyat) {
        this.extraFiyat = extraFiyat;
    }

    @Override
    public Long getVehicle_Id() {
        return vehicle_Id;
    }

    @Override
    public void setVehicle_Id(Long vehicle_Id) {
        this.vehicle_Id = vehicle_Id;
    }

    public Long getOffer_Id() {
        return offer_Id;
    }

    public void setOffer_Id(Long offer_Id) {
        this.offer_Id = offer_Id;
    }
}
