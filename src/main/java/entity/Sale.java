package entity;

import jakarta.persistence.*;

import java.util.Date;

public class Sale {

    @Id
    @SequenceGenerator(initialValue = 1, name = "idGenerator", sequenceName = "SEQ_Sale")
    @GeneratedValue(generator = "idGenerator", strategy = GenerationType.AUTO)
    @Column(name = "SaleId", unique = true, nullable = false, columnDefinition = "long")
    private Long saleId;

    @Column(name = "vehicle_Id",columnDefinition = "long")
    private Long vehicle_Id;//hangi arac satildi

    @Column(name = "userId",columnDefinition = "long")
    private Long userId;///kim satin aldi

    @Column(name = "sale_date",columnDefinition = "datetime")
    private Date sale_date;

    public Sale() {
    }

    public Long getSaleId() {
        return saleId;
    }

    public void setSaleId(Long saleId) {
        this.saleId = saleId;
    }

    public Long getVehicle_Id() {
        return vehicle_Id;
    }

    public void setVehicle_Id(Long vehicle_Id) {
        this.vehicle_Id = vehicle_Id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getSale_date() {
        return sale_date;
    }

    public void setSale_date(Date sale_date) {
        this.sale_date = sale_date;
    }
}
