package controller;

import entity.User;
import entity.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import service.SaleService;

import java.util.List;

@Controller
@RestController
public class SaleController {

    @Autowired
    SaleService saleService;

    @PostMapping("/findMostPurchasedVehicles")
    public List<Vehicle> findMostPurchasedVehicles(Long userId) {
        return saleService.findMostPurchasedVehicles(userId);
    }

    @PostMapping("/seeMostPurchasedProducts")
    public List<Vehicle> seeCustomerProduct(Long userId) {

        List<Vehicle> customerVehicle = saleService.seeCustomerProduct(userId);
        return (List<Vehicle>) ResponseEntity.ok(customerVehicle);
    }

}
