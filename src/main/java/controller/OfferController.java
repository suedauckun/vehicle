package controller;

import entity.AdditionalOffer;
import entity.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import service.AdditionalOfferService;
import service.OfferService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RestController
public class OfferController {


    @Autowired
    OfferService offerService;
    @Autowired
    AdditionalOfferService additionalOfferService;

    @PostMapping("/offerSave")
    public ResponseEntity<Offer> save(@RequestBody Offer offer){
        Offer savedOffer = offerService.save(offer);
        return new ResponseEntity<Offer>(savedOffer, HttpStatus.CREATED);
    }

    @GetMapping("/offersAll")
    public ResponseEntity<List<Offer>> getAllOffer(){
        List<Offer> allOffer = offerService.getAllOffer();
        return new ResponseEntity<List<Offer>>(allOffer, HttpStatus.OK);
    }

    @GetMapping("/usersOffer{id}")///*Kullanicilar teklifleri görebilir *////
    public ResponseEntity<Offer> getOfferByUserId(@PathVariable(name = "id") Long userId){
        Offer offer = offerService.getOfferByUserId(userId);
        return new ResponseEntity<Offer>(offer, HttpStatus.OK);
    }

    @PutMapping("/offerUpdate/{id}")
    public ResponseEntity<Offer> updateOffer(@RequestBody Offer offer){
        Offer updatedOffer = offerService.updateOffer(offer);
        return new ResponseEntity<Offer>(updatedOffer, HttpStatus.OK);
    }

    @DeleteMapping("/offerDelete/{id}")
    public ResponseEntity<Void> deleteOfferById(@PathVariable(name = "id") Long offerId){
        offerService.deleteOfferById(offerId);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }

    @PostMapping("/additonalOfferSave")
    public ResponseEntity<AdditionalOffer> save(@RequestBody AdditionalOffer additionalOffer){
        AdditionalOffer savedAdditonalOffer = additionalOfferService.save(additionalOffer);
        return new ResponseEntity<AdditionalOffer>(savedAdditonalOffer, HttpStatus.CREATED);
    }

    @GetMapping("/additonalOfferAll")
    public ResponseEntity<List<AdditionalOffer>> getAllAdditonalOffer(){
        List<AdditionalOffer> allAdditonalOffer = additionalOfferService.getAllOffer();
        return new ResponseEntity<List<AdditionalOffer>>(allAdditonalOffer, HttpStatus.OK);
    }

    @PutMapping("/additonalOfferUpdate/{id}")
    public ResponseEntity<AdditionalOffer> updateAdditonalOffer(@RequestBody AdditionalOffer additionalOffer){
        AdditionalOffer updatedOffer = additionalOfferService.updateOffer(additionalOffer);
        return new ResponseEntity<AdditionalOffer>(updatedOffer, HttpStatus.OK);
    }

    @DeleteMapping("/additonalOfferDelete/{id}")
    public ResponseEntity<Void> deleteAdditonalOfferById(@PathVariable(name = "id") Long additionalOfferId){
        additionalOfferService.deleteOfferById(additionalOfferId);
        return new ResponseEntity<Void>(HttpStatus.ACCEPTED);
    }

}
